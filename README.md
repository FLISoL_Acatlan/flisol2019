# FLISoL 2019

Presentaciones de las charlas y talleres.

---

- Charlas
	- [Debian y su misión con el software libre](./charlas/debianysumisiónconelSL/presentacion.pdf)
	- [Diseñadora Gráfica: Migrando mi hogar a Linux](./charlas/dg:migrandomihogaralinux/presentacion.pdf)
	- [Generando valor a través del Software Libre](./charlas/generando_valor_a_traves_del_sl/presentacion.pdf)
	- [Linux contra la basura electrónica](./charlas/linuxvsbasuraelectrónica/presentacion.pdf)
	- [Rho-Pi: Bot de Telegram para controlar una RaspberryPi](./charlas/rho-pi-bot_de_telegram_p_controlar_raspberry/presentacion.pdf)
	- [Vampiros vs Hombres Lobo. La cultura DevOps](./charlas/vampirosvshombreslobo/presentacion.pdf)
	- [Fedora containers](./charlas/fedora_containers/presentacion.pdf)

---

- Talleres
	- [Introducción a Laravel](./talleres/laravel/presentacion.pdf)
	- [LaTeX básico](https://github.com/Vic06Hug/flisol2019LaTeXbasico)
	- [LaTeX académico](https://github.com/Vic06Hug/FISoL2019LaTeXAcademico)

